<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();
            $table->string("name", 50)->uniqid();
            $table->string("slug")->uniqid();
            $table->integer("price")->default(250);
            $table->integer("wallet");
            $table->integer("chair");
            $table->boolean("active")->default(false);
            $table->foreignId('category_id')->constrained('categories');
            $table->foreignId('energy_id')->constrained('energies');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
