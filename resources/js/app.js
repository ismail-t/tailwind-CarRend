require('./bootstrap');

import Alpine from 'alpinejs'

window.Alpine = Alpine

window.Swiper = require('swiper');
import 'swiper/css';

Alpine.start()
