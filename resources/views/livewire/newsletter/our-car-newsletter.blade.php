<div>
    <x-cards.card-our-car title="newsletter">
        <form>
            <div class="relative">
                @error('email')
                    <span class="absolute top-1 right-2.5 text-[12px] text-red-500 font-light">{{ $message }}</span>
                @enderror

                <input wire:model="email" type="text" class="w-full py-5 px-6 bg-white placeholder-second-gray text-sm" placeholder="Enter Your E-mail">
            </div>

            <button class="mt-6 w-full py-4 bg-primary text-white">subscripe</button>
        </form>
    </x-cards.card-our-car>
</div>
