<div>
    <livewire:car.header />

    <div class="w-full lg:w-90 mx-auto pt-20 pb-20">
        <h1 class="mb-10 text-center text-[28px] lg:text-4xl capitalize font-medium text-gray-800 tracking-wider">first-car car rental</h1>

        <div class="w-full md:px-5 lg:px-0 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-y-8">
            @foreach ($cars as $car)
                <x-cars.first-class.first-class :car="$car"/>
            @endforeach
        </div>
    </div>
</div>
