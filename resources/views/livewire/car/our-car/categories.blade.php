<div>
    <x-cards.card-our-car title="categories" >
        <div class="flex flex-col pl-14 lg:pl-16 pt-4 space-y-4">
            <a href="#" wire:prevent="#" class="relative capitalize tracking-wide font-medium text-primary before:absolute before:w-8 lg:before:w-10 before:h-[3px] before:-left-12 before:top-1/2 before:-translate-y-1/2 before:bg-primary">all categories</a>
            <a href="#" wire:prevent="#" class="capitalize tracking-wide">BMW</a>
            <a href="#" wire:prevent="#" class="capitalize tracking-wide">audi</a>
            <a href="#" wire:prevent="#" class="capitalize tracking-wide">porsche</a>
            <a href="#" wire:prevent="#" class="capitalize tracking-wide">tesla</a>
        </div>
    </x-cards.card-our-car>
</div>
