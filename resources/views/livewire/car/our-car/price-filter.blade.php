<div>
    <x-cards.card-our-car title="filter by price" >
        <div class="py-5 flex flex-wrap gap-4">
            <button class="relative pl-3 pr-2 py-1.5 overflow-hidden tracking-wider text-second-gray bg-white before:absolute before:top-1/2 before:-translate-y-1/2 before:left-0 before:-translate-x-1/2 before:w-5 before:h-5 before:rounded-full before:bg-five-gray">$0-$100</button>
            <button class="relative pl-3 pr-2 py-1.5 overflow-hidden tracking-wider text-second-gray bg-white before:absolute before:top-1/2 before:-translate-y-1/2 before:left-0 before:-translate-x-1/2 before:w-5 before:h-5 before:rounded-full before:bg-five-gray">$100-$220</button>
            <button class="relative pl-3 pr-2 py-1.5 overflow-hidden tracking-wider text-second-gray bg-white before:absolute before:top-1/2 before:-translate-y-1/2 before:left-0 before:-translate-x-1/2 before:w-5 before:h-5 before:rounded-full before:bg-five-gray">$200-$400</button>
            <button class="relative pl-3 pr-2 py-1.5 overflow-hidden tracking-wider text-second-gray bg-white before:absolute before:top-1/2 before:-translate-y-1/2 before:left-0 before:-translate-x-1/2 before:w-5 before:h-5 before:rounded-full before:bg-five-gray">$400-$1k</button>
        </div>
    </x-cards.card-our-car>
</div>
