<div>
    <x-header image="bg-gallery.jpg" >
        <div class="relative w-full py-16 lg:py-28 px-4 space-y-12">
            <h1 class="capitalize text-center text-3xl lg:text-5xl font-medium tracking-[5px] leading-none text-white">car categories</h1>
        </div>
    </x-header>

    <livewire:car.car-categories />
</div>
