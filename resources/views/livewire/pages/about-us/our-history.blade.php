<div>
    <section class="py-8 lg:py-20 md:px-5 lg:px-14">
        <h1 class="mb-12 text-center text-[28px] md:text-3xl lg:text-4xl text-second-gray capitalize font-medium">our history</h1>

        <div class="swiperOurHistory w-full overflow-hidden">
            <div class="swiper-wrapper">
                <x-pages.about-us.our-history.slide-car-item image="first-car-1.jpg" link="#" name="Establishment of Rental Car" year="1995" />

                <x-pages.about-us.our-history.slide-car-item image="first-car-2.jpg" link="#" name="Opening offices in Illinois" year="2005" />

                <x-pages.about-us.our-history.slide-car-item image="first-car-3.jpg" link="#" name="Atrackting new partners" year="2010" />

                <x-pages.about-us.our-history.slide-car-item image="first-car-4.jpg" link="#" name="Gaining national recognition" year="2018" />
            </div>
        </div>
    </section>
</div>
