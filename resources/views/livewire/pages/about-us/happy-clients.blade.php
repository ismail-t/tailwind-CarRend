<div class="w-full py-10">
    <div class="w-full lg:w-84 mx-auto px-4 md:px-8 lg:px-0">
        <h1 class="capitalize text-center text-3xl md:text-4xl font-medium text-gray-800">happy clients</h1>

        <div class="relative mt-[70px] w-full h-[550px] before:absolute before:inset-x-9 before:-top-4 before:h-4 before:bg-forth-gray after:absolute after:inset-x-5 after:-top-2 after:h-2 after:bg-five-gray">
            <x-comments.about-us.index-happy-clients />
        </div>
    </div>
</div>
