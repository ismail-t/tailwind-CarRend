<div class="w-full py-16">
    <div class="w-full xl:w-84 mx-auto px-4 md-px-6 lg:px-0">
        <h1 class="mb-10 text-center text-[28px] lg:text-4xl capitalize font-medium text-gray-800 tracking-wider">popular cars</h1>

        <x-cars.popular-cars.index-welcome :cars="$this->cars" />
    </div>
</div>
