<div class="w-full lg:w-90 mx-auto pt-20 pb-20">
    <h1 class="mb-10 text-center text-[28px] lg:text-4xl capitalize font-medium text-gray-800 tracking-wider">first-car car rental</h1>

    <x-cars.first-class.index :categories="$this->categories" />
</div>
