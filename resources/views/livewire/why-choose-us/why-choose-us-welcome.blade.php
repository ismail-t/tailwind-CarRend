<div class="w-full bg-second-gray py-16 md:pt-20 text-white">
    <div class="w-full lg:w-84 mx-auto px-4 md:px-6 lg:px-0">
        <h1 class="capitalize text-center text-[28px] md:text-4xl font-medium tracking-wide">why choose us?</h1>

        <x-pages.why-choose-us.index />
    </div>
</div>
