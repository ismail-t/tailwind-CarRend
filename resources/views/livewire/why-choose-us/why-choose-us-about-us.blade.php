<div class="w-full xl:w-[1190px] mx-auto pt-14 md:pt-24 pb-8">
    <div class="px-4 md:px-20 lg:px-0 lg:flex lg:items-start">
        <div class="w-full mx-auto lg:ml-0 lg:w-1/2 lg:pr-20">
            <img src="{{ asset('imgs/about-1.jpg') }}" class="w-full object-cover" alt="">
        </div>

        <div
            x-data="{ aboutUs: true, ourValues: false, mission: false}"
            class="pt-11 lg:pt-[70px] lg:pr-8 lg:w-1/2"
        >
            <h1 class="mb-8 lg:mb-16 text-center md:text-left text-[28px] md:text-4xl capitalize font-medium text-gray-800 tracking-wider md:tracking-wide">why choose us</h1>

            <ul class="mx-auto flex flex-wrap justify-center md:justify-between md:pb-1.5 capitalize text-xl text-third-gray md:border-b-4 md:border-forth-gray">
                <li class="mb-8 md:mb-0">
                    <a
                        href="#"
                        @click.prevent="aboutUs= true, ourValues= false, mission= false"
                        x-bind:class=" aboutUs ? '!text-blue-500 !border-blue-500' : '' "
                        class="px-2.5 lg:px-0 pt-2 pb-1.5 md:pt-0 tracking-wider border-t md:border-t-0 border-b md:border-b-4 border-forth-gray transition duration-500 ease-out hover:text-blue-500"
                    >about us</a>
                </li>

                <li class="mb-8 md:mb-0">
                    <a
                        href="#"
                        @click.prevent="aboutUs= false, ourValues= true, mission= false"
                        x-bind:class=" ourValues ? '!text-blue-500 !border-blue-500' : '' "
                        class="px-2.5 lg:px-0 pt-2 pb-1.5 md:pt-0 tracking-wider border-t md:border-t-0 border-b md:border-b-4 border-forth-gray transition duration-500 ease-out hover:text-blue-500"
                    >our values</a>
                </li>

                <li class="mb-8 md:mb-0">
                    <a
                        href="#"
                        @click.prevent="aboutUs= false, ourValues= false, mission= true"
                        x-bind:class=" mission ? '!text-blue-500 !border-blue-500' : '' "
                        class="px-2.5 lg:px-0 pt-2 pb-1.5 md:pt-0 tracking-wider border-t md:border-t-0 border-b md:border-b-4 border-forth-gray transition duration-500 ease-out hover:text-blue-500"
                    >mission</a>
                </li>
            </ul>

            <x-sections.why-choose-us.about-us />

            <x-sections.why-choose-us.our-values />

            <x-sections.why-choose-us.mission />

            <div x-show="!mission" class="mt-6 flex flex-col md:flex-row justify-center md:justify-start space-y-5 md:space-y-0 md:space-x-5 items-center">
                <x-buttons.button-about-us link="#" title="read more" class="lg:w-60 text-white bg-blue-500 hover:bg-gray-400" />
                <x-buttons.button-about-us link="/contact-us" title="contact us" class="lg:w-80 h-12 lg:h-14 text-second-gray bg-white border-2 border-second-gray hover:text-white hover:bg-blue-500 hover:border-blue-500" />
            </div>
        </div>
    </div>
</div>
