<div class="w-full py-14 md:py-24">
    <div class="w-full mx-auto px-4 lg:px-0">
        <h1 class="capitalize text-center text-3xl md:text-4xl font-medium text-gray-800">how we work</h1>

        <x-sections.work-strategy.index-how-we-work />
    </div>
</div>
