<div class="w-full py-16 md:py-20 bg-second-gray">
    <div class="w-full lg:w-84 mx-auto px-4 lg:px-0">
        <div class="flex items-center flex-wrap lg:flex-nowrap lg:items-start">
            <div class="w-full md:w-1/2 lg:w-full md:px-3">
                <img src="{{ asset('imgs/count-cars.jpg') }}" class="w-full object-cover">
            </div>

            <div class="w-full md:w-1/2 lg:w-full mt-12 px-5 lg:pr-32 space-y-8 md:mt-0 text-white">
                <h1 class="pl-5 py-0.5 text-[40px] font-medium tracking-wide border-l-4 border-blue-600">15+ Years of Experience</h1>
                <p class="text-[15px] font-medium text-first-gray leading-6">With more than 15 years of experience, our team offers quality car rental service to all US guests and residents.</p>
                <button class="uppercase text-sm px-7 py-3 bg-blue-500 hover:bg-third-gray">find a car</button>
            </div>

            <x-sections.general-state.index-general-state />
        </div>

        <livewire:common.type />
    </div>
</div>
