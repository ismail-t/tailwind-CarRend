<div class="w-full bg-five-gray py-14 md:pt-28 md:pb-24">
    <div class="w-full lg:w-84 mx-auto px-4 md:px-6 lg:px-0">
        <h1 class="text-center text-[28px] lg:text-4xl capitalize font-medium text-gray-800 tracking-wider">find your vehicle <span class="lowercase">by</span> type</h1>

        <x-cars.find-your-vehicle-by-type.index-welcome />
    </div>
</div>
