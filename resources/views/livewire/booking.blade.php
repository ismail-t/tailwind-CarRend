<div>
    @if ($displayBookingForm)

    <div class="absolute w-full px-5 py-5 @if ($displayBookingForm) -translate-y-12 transition duration-500 @endif drop-shadow-lg bg-white">
        <span wire:click="hideForm" class="absolute p-2 top-0 right-0 z-10 text-2xl font-medium drop-shadow-lg">
            x
        </span>

        {{-- <h1 class="text-base text-black font-normal tracking-wide">Rental Car <span class="uppercase text-lg text-primary">{{ $car->name }}</span></h1> --}}

        {{-- <div>
            <h4 class="text-xl font-medium capitalize">start:</h4>

            <div class="flex justify-start items-center space-x-4">
                <input wire:model="state.start_day" type="text" pattern="[0-9]{1}" class="px-4 py-1 w-14 text-lg font-medium tracking-wide placeholder:text-lg placeholder:font-light placeholder:text-slate-700" placeholder="d">

                <span>/</span>

                <input wire:model="state.start_month" type="text" pattern="[0-9]{1}" class="px-4 py-1 w-14 text-lg font-medium tracking-wide placeholder:text-lg placeholder:font-light placeholder:text-slate-700" placeholder="m">

                <span>/</span>

                <input wire:model="state.start_year" type="text" pattern="[0-9]{1}" class="px-4 py-1 w-20 text-lg font-medium tracking-wide placeholder:text-lg placeholder:font-light placeholder:text-slate-700" placeholder="y">
            </div>

            @error('state.start_day')
                {{ $message }}
            @enderror
        </div>

        <div>
            <h4 class="text-xl font-medium capitalize">end:</h4>

            <div class="flex justify-start items-center space-x-4">
                <input wire:model="state.end_day" type="text" pattern="[0-9]{1}" class="px-4 py-1 w-14 text-lg font-medium tracking-wide placeholder:text-lg placeholder:font-light placeholder:text-slate-700" placeholder="d">

                <span>/</span>

                <input wire:model="state.end_month" type="text" pattern="[0-9]{1}" class="px-4 py-1 w-14 text-lg font-medium tracking-wide placeholder:text-lg placeholder:font-light placeholder:text-slate-700" placeholder="m">

                <span>/</span>

                <input wire:model="state.end_year" type="text" pattern="[0-9]{1}" class="px-4 py-1 w-20 text-lg font-medium tracking-wide placeholder:text-lg placeholder:font-light placeholder:text-slate-700" placeholder="y">
            </div>
        </div> --}}

        <div>
            <h4 class="text-xl font-medium capitalize">start:</h4>

            @error('state.start_date')
                {{ $message }}
            @enderror

            <div class="flex justify-start items-center space-x-4">
                <input wire:model="state.start_date" type="date" class="px-4 py-1 w-full text-lg font-medium tracking-wide placeholder:text-lg placeholder:font-light placeholder:text-slate-700" placeholder="d">
            </div>
        </div>

        <div>
            <h4 class="text-xl font-medium capitalize">end:</h4>

            @error('state.end_date')
                {{ $message }}
            @enderror

            <div class="flex justify-start items-center space-x-4">
                <input wire:model="state.end_date" type="date" class="px-4 py-1 w-full text-lg font-medium tracking-wide placeholder:text-lg placeholder:font-light placeholder:text-slate-700" placeholder="d">
            </div>
        </div>

        @if ($displayContract)
            <div class="mt-6 w-full">
                <ul class="divide-y divide-blue-500">
                    <li class="grid grid-cols-2 py-1.5">
                        <h1 class="capitalize">car :</h1>
                        <h1 class="uppercase text-lg font-medium text-primary">{{$car->name}}</h1>
                    </li>
                    <li class="grid grid-cols-2 py-1.5">
                        <h1 class="capitalize">days :</h1>
                        <h1 class="uppercase text-lg font-medium text-primary">{{$this->daysRental}}<span class="lowercase text-first-gray"> /@if($this->daysRental < 2) day @else days @endif</span></h1>
                    </li>
                    <li class="grid grid-cols-2 py-1.5">
                        <h1 class="capitalize">price :</h1>
                        <h1 class="uppercase text-lg font-medium text-primary">{{$this->price}} $</h1>
                    </li>
                </ul>
            </div>
        @endif

        <div class="flex justify-start items-end space-x-5">
            @if ($confirmate)
                <button wire:click="rentalCar()" class="mt-10 px-5 py-1 text-xl font-medium tracking-widest bg-primary text-white">rental car</button>
            @else
                <button wire:click="next()" class="mt-10 px-5 py-1 text-lg font-medium tracking-widest bg-primary text-white">next</button>
            @endif

            <span wire:click="hideForm" class="cursor-pointer text-third-gray">cancel</span>
        </div>
    </div>

    @endif
</div>
