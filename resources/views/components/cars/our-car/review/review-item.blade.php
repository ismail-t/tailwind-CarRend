@props(['message', 'username', 'carName'])

<div class="py-4">
    <p class="relative pl-7 lg:pl-14 text-base tracking-wide leading-6 before:absolute before:content-['\201C'] before:top-1 before:left-0 before:text-primary before:font-medium before:text-4xl">{{ $message }}</p>
    <div class="mt-5 uppercase text-sm font-medium">
        <h6 class="text-third-gray">{{ $username }}</h6>
        <h6 class="text-primary">{{ $carName }}</h6>
    </div>
</div>
