@props(['image', 'name'])

<div class="w-full h-60">
    <a href="#" class="w-full h-48 block overflow-hidden">
        <img src="{{ asset('imgs/' . $image) }}" class=" w-full h-full" alt="{{ $name }}">
    </a>
    <h1 class="mt-4 capitalize text-second-gray tracking-wide">{{ $name }}</h1>
</div>
