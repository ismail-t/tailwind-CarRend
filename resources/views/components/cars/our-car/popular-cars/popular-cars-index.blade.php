<div class="space-y-4">
    <x-cars.our-car.popular-cars.popular-car-item name="audi" image="first-car-1.jpg" />

    <x-cars.our-car.popular-cars.popular-car-item name="Mercedes-Benz C218" image="first-car-2.jpg" />
</div>
