@props(['car'])

<a href="{{ $car->slug }}" class="group flex flex-col lg:flex-row p-4 border border-forth-gray">
    <div class="overflow-hidden block">
        <img src="{{ $car->image }}" class="w-[430px] object-cover scale-110 mb-10 lg:mb-0 group-hover:transition group-hover:duration-300 group-hover:scale-100">
    </div>

    <div class="w-full flex flex-col lg:flex lg:flex-col lg:justify-center lg:pl-8 lg:pr-20">
        <div class="flex flex-row space-x-4">
            <div class="flex space-x-4 text-third-gray">
                <svg class="fill-current w-4" viewBox="0 0 512 512"><g><g><path d="M380.9,51.2c-2.5-8.7-10.5-14.7-19.6-14.7H150.7c-9.1,0-17,6-19.6,14.7L99.8,158.1h312.4L380.9,51.2z"/><path d="m491.6,252.6l-75.2-48.5c-3.3-2.1-7.1-3.3-11.1-3.3h-301.7c-4.1,0-8,1.2-11.4,3.5l-72.2,48.5c-5.7,3.8-9,10.1-9,17v121.1c0,11.3 9.1,20.4 20.4,20.4h53.4v43.8c0,11.3 9.1,20.4 20.4,20.4 11.3,0 20.4-9.1 20.4-20.4v-43.8h260.7v43.8c0,11.3 9.1,20.4 20.4,20.4 11.3,0 20.4-9.1 20.4-20.4v-43.8h53.4c11.3,0 20.4-9.1 20.4-20.4v-121.1c0.1-7-3.4-13.5-9.3-17.2zm-383.7,94.5c-11.3,0-20.5-9.2-20.5-20.5s9.2-20.5 20.5-20.5 20.5,9.2 20.5,20.5-9.1,20.5-20.5,20.5zm210-.2h-123.8c-11.3,0-20.4-9.1-20.4-20.4 0-11.3 9.1-20.4 20.4-20.4h123.8c11.3,0 20.4,9.1 20.4,20.4 5.68434e-14,11.3-9.1,20.4-20.4,20.4zm86.2,.2c-11.3,0-20.5-9.2-20.5-20.5 0-11.3 9.2-20.5 20.5-20.5s20.5,9.2 20.5,20.5c0,11.4-9.2,20.5-20.5,20.5z"/></g></g></svg>
                <h4 class="uppercase text-sm lg:text-bassse lg:font-medium text-black">{{ $car->energy->name }}</h4>
            </div>

            <div class="flex space-x-4 text-third-gray">
                <svg class="fill-current w-4" x="0px" y="0px" viewBox="0 0 258.75 258.75"><g><circle cx="129.375" cy="60" r="60"/><path d="M129.375,150c-60.061,0-108.75,48.689-108.75,108.75h217.5C238.125,198.689,189.436,150,129.375,150z"/></g></svg>
                <h4 class="uppercase text-sm lg:font-medium text-black">{{ $car->chair }}</h4>
            </div>

            <div class="flex space-x-4 text-third-gray">
                <svg class="fill-current w-4" x="0px" y="0px" viewBox="0 0 31.986 31.986"><g><g><path d="M0,18.433v8.05c0,1.354,1.1,2.453,2.455,2.453h27.077c1.356,0,2.454-1.099,2.454-2.453v-8.125l-15.989,2.252L0,18.433z"/><path d="M22.749,7.583V7.565V5.507c0-1.357-1.101-2.456-2.455-2.456h-9.123c-1.356,0-2.455,1.099-2.455,2.456v2.058v0.018H2.455C1.1,7.583,0,8.682,0,10.039v6.002l15.99,2.177l15.996-2.255v-5.924c0-1.356-1.1-2.456-2.454-2.456H22.749z M19.611,12.289v2.963h-7.147v-2.963V11.68h7.147V12.289z M20.395,7.565v0.018h-9.327V7.565V6.687c0-0.708,0.573-1.281,1.281-1.281h6.764c0.709,0,1.281,0.574,1.281,1.281V7.565L20.395,7.565z"/></g></g></svg>
                <h4 class="uppercase text-sm lg:font-medium text-black">{{ $car->wallet }}</h4>
            </div>
        </div>

        <h1 class="mt-4 capitalize text-2xl text-black font-medium">{{ $car->name }}</h1>

        <div class="relative mt-3 pl-4 pr-1 py-1 w-[100px] bg-primary text-sm font-medium text-white before:absolute before:w-4 before:h-4 before:rounded-full before:bg-white before:-left-2 before:top-1.5">{{ $car->priceDay }}</div>
    </div>
</a>
