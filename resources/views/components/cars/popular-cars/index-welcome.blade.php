@props(['cars'])


<div class="w-full grid grid-cols-1 md:grid-cols-2 gap-y-8 md:gap-6">
    @foreach ($cars as $car)

    <x-cars.popular-cars.popular-car-welcome :car="$car" />

    @endforeach
</div>
