@props(['image', 'name', 'status', 'body'])

<div class="swiper-slide py-10 bg-white shadow-lg">
    <div class="flex justify-center items-center">
        <img src="{{ asset('imgs/' . $image) }}" class="w-24 h-24 rounded-full" alt="">
    </div>

    <div class="relative mt-5 w-full px-8 before:absolute before:content-['\201C'] before:top-0 before:left-3 before:text-primary before:font-medium before:text-3xl after:absolute after:content-['\201C'] after:bottom-0 after:right-3 after:text-primary after:font-medium after:text-3xl">
        <p class="text-xl text-center font-light text-black leading-10 tracking-wider">
            {{ $body }}
        </p>
    </div>

    <div class="mt-5 text-center">
        <h1 class="capitalize text-second-gray text-xl tracking-wider">{{ $name }}</h1>
        <p class="text-blue-400 text-sm">{{ $status }}</p>
    </div>
</div>
