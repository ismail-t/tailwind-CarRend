@props(['title'])

<div class="relative p-4 pt-16 bg-five-gray">
    <div class="absolute top-0 left-0 p-2 pr-5 pl-1 bg-white">
        <h1 class="tracking-widest font-medium uppercase text-lg lg:text-xl">{{ $title }}</h1>
    </div>

    {{ $slot }}
</div>
