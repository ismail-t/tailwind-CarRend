@props(['type', 'numberCars', 'link'])

<div class="relative w-full flex-shrink pt-16 lg:pt-20 pb-6 flex flex-col items-center text-gray-800 bg-white border border-forth-gray">
    <div class="absolute -top-1/2 rounded-full bg-white border-[12px] border-five-gray p-6">
        {{ $slot }}
    </div>
    <h3 class="capitalize text-2xl font-medium text-center text-gray-800 tracking-wider">{{ $type }}</h3>
    <h4 class="text-blue-500 text-base">{{ $numberCars }} cars</h4>
    <a href="{{ $link }}" class="uppercase text-first-gray underline">view</a>
</div>
