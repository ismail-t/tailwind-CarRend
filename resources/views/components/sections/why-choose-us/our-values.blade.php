<div
    x-show="ourValues"
    class="my-2 md:mt-4"
>
    <div class="pb-4 w-full flex flex-col md:flex-row flex-wrap md:justify-between items-center text-second-gray">
        <div class="relative w-48 lg:w-40">
        {{-- <div class="relative w-56 before:bg-yellow-500 before:absolute before:top-1/2 before:-translate-y-1/2 before:left-1/2 before:-translate-x-1/2 before:w-11/12 before:h-44 before:border-8 before:border-gray-500 before:rounded-full"> --}}
            <canvas id="dedication" class="w-full h-40"></canvas>
            <h2 class="text-center absolute top-1/2 -translate-y-1/2 left-1/2 -translate-x-1/2 text-2xl lg:text-5xl">87%</h2>
            <h1 class="uppercase text-center text-xl md:text-lg tracking-wider">dedication</h1>
        </div>

        <div class="relative w-48 lg:w-40">
        {{-- <div class="relative w-56 before:bg-yellow-500 before:absolute before:top-1/2 before:-translate-y-1/2 before:left-1/2 before:-translate-x-1/2 before:w-11/12 before:h-44 before:border-8 before:border-gray-500 before:rounded-full"> --}}
            <canvas id="knowledge" class="w-full h-40"></canvas>
            <h2 class="text-center absolute top-1/2 -translate-y-1/2 left-1/2 -translate-x-1/2 text-2xl lg:text-5xl">74%</h2>
            <h1 class="uppercase text-center text-xl md:text-lg tracking-wider">knowledge</h1>
        </div>

        <div class="relative w-48 lg:w-40">
        {{-- <div class="relative w-56 before:bg-yellow-500 before:absolute before:top-1/2 before:-translate-y-1/2 before:left-1/2 before:-translate-x-1/2 before:w-11/12 before:h-44 before:border-8 before:border-gray-500 before:rounded-full"> --}}
            <canvas id="professional" class="w-full h-40"></canvas>
            <h2 class="text-center absolute top-1/2 -translate-y-1/2 left-1/2 -translate-x-1/2 text-2xl lg:text-5xl">99%</h2>
            <h1 class="uppercase text-center text-xl md:text-lg tracking-wider">professionalism</h1>
        </div>
        <script>
            // dedication
            var myCanvas = document.querySelector("#dedication");
            dedication = myCanvas.getContext('2d');

            dedication.beginPath();

            dedication.lineWidth = 13;

            dedication.strokeStyle = '#e95454';

            dedication.arc(140,80,60, 0 , (2/100 * 87) * Math.PI);

            dedication.stroke();


            var myCanvas = document.querySelector("#knowledge");
            knowledge = myCanvas.getContext('2d');

            knowledge.beginPath();

            knowledge.lineWidth = 13;

            knowledge.strokeStyle = '#dfa234';

            knowledge.arc(140,80,60, 0 , (2/100 * 74) * Math.PI);

            knowledge.stroke();


            var myCanvas = document.querySelector("#professional");
            professional = myCanvas.getContext('2d');

            professional.beginPath();

            professional.lineWidth = 13;

            professional.strokeStyle = '#0fab4e';

            professional.arc(140,80,60, 0 , (2/100 * 99) * Math.PI);

            professional.stroke();
        </script>
    </div>
</div>
