<div
    x-show="aboutUs"
    class="mt-4 md:mt-8"
>
    <p class="text-sm tracking-wider leading-7 lg:leading-6 text-second-gray">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Assumenda distinctio accusantium unde ut debitis quidem quas quasi eos atque sint possimus eius, adipisci nobis cumque ad laboriosam quis odio tenetur.</p>

    <ul class="mt-10 md:mt-6 w-full flex flex-wrap text-sm text-second-gray">
        <li class="relative w-1/2 lg:w-1/3 pl-4 mb-4 md:mb-5 before:absolute before:-translate-y-1/2 before:top-1/2 before:left-0 before:w-2 before:h-2 before:bg-blue-500 before:rounded-full">
            <h5>Sedans</h5>
        </li>

        <li class="relative w-1/2 lg:w-1/3 pl-4 mb-4 md:mb-5 before:absolute before:-translate-y-1/2 before:top-1/2 before:left-0 before:w-2 before:h-2 before:bg-blue-500 before:rounded-full">
            <h5>SUVs</h5>
        </li>

        <li class="relative w-1/2 lg:w-1/3 pl-4 mb-4 md:mb-5 before:absolute before:-translate-y-1/2 before:top-1/2 before:left-0 before:w-2 before:h-2 before:bg-blue-500 before:rounded-full">
            <h5>Buses</h5>
        </li>

        <li class="relative w-1/2 lg:w-1/3 pl-4 mb-4 md:mb-5 before:absolute before:-translate-y-1/2 before:top-1/2 before:left-0 before:w-2 before:h-2 before:bg-blue-500 before:rounded-full">
            <h5>Hatchbacks</h5>
        </li>

        <li class="relative w-1/2 lg:w-1/3 pl-4 mb-4 md:mb-5 before:absolute before:-translate-y-1/2 before:top-1/2 before:left-0 before:w-2 before:h-2 before:bg-blue-500 before:rounded-full">
            <h5>MVPs</h5>
        </li>

        <li class="relative w-1/2 lg:w-1/3 pl-4 mb-4 md:mb-5 before:absolute before:-translate-y-1/2 before:top-1/2 before:left-0 before:w-2 before:h-2 before:bg-blue-500 before:rounded-full">
            <h5>Electric Cars</h5>
        </li>
    </ul>
</div>
