<ul class="relative w-full md:mt-16 lg:mt-0 text-white uppercase px-8 lg:px-0 grid grid-cols-1 md:grid-cols-2 items-center md:justify-center text-center">
    <x-sections.general-state.item-general-state number="20" title="hapy clients" />

    <x-sections.general-state.item-general-state number="14" title="car" />

    <x-sections.general-state.item-general-state number="10" title="car type" />

    <x-sections.general-state.item-general-state number="16" title="brands" />

    <svg class="fill-current w-8 absolute top-1/2 -translate-y-1/2 left-1/2 -translate-x-1/2"  x="0px" y="0px" viewBox="0 0 485 485" style="enable-background:new 0 0 485 485;" xml:space="preserve"><polygon points="485,227.5 257.5,227.5 257.5,0 227.5,0 227.5,227.5 0,227.5 0,257.5 227.5,257.5 227.5,485 257.5,485 257.5,257.5 485,257.5 "/></svg>
</ul>
