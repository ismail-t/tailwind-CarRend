@props(['number', 'title'])

<li class="py-6 w-full md:w-2/3 lg:w-full md:justify-self-end border-b lg:border-none border-first-gray">
    <h1 class="text-6xl">{{ $number }}<span class="text-4xl">k</span></h1>
    <h3 class="mt-2 text-sm">{{ $title }}</h3>
</li>
