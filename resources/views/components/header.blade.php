@props(['image'])

<div class="relative w-full lg:py-16">
    <img src="{{ asset('imgs/' . $image) }}" class="absolute w-full h-full object-cover inset-0" alt="">
    <div class="absolute inset-0  bg-gray-600/40"></div>

    {{ $slot }}

</div>
