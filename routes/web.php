<?php

use App\Http\Livewire\Car\AddCar;
use Illuminate\Support\Facades\Route;
use App\Http\Livewire\Pages\Welcome;
use App\Http\Livewire\Pages\AboutUs;
use App\Http\Livewire\Car\CarIndex;
use App\Http\Livewire\Car\CarInfo;
use App\Http\Livewire\Car\DisplayResultCar;
use App\Http\Livewire\Car\OurCarIndex;
use App\Http\Livewire\ContactIndex;
use App\Http\Livewire\Vendor\BookingCar;
use App\Http\Livewire\Vendor\BookingInformationCar;
use App\Models\Car;
use App\Models\Category;
use App\Models\Image;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/car/add', function(){
    // $car = Car::create([
    //     'name' => "volkswagen tiguan",
    //     'slug' => Str::slug("volkswagen-tiguan") . "-" . date("Ymd"),
    //     'wallet' => 4,
    //     // 'number' => 2,
    //     'chair' => 4,
    //     'category_id' => 10,
    //     'energy_id' => 3
    // ]);

    // $car->images()->create([
    //     "name" => "volkswagen-tiguan.jpg"
    // ]);

    // $car->images()->create([
    //     "name" => "volkswagen-tiguan-2.jpg"
    // ]);

    // $car->images()->create([
    //     "name" => "dacia-sandero-3.jpg"
    // ]);

    // dd($car, $car->images);
// });

Route::get('/car/add', AddCar::class);

Route::get('category/cars', function(){
    $categories = Category::with('cars')->get();

    dd($categories);
});

Route::get('/ww', function () {
    return view('welcome');
});


Route::get('/', Welcome::class )->name('home');

Route::get('/about-us',AboutUs::class)->name('about-us');

Route::group(['prefix' => 'cars' ,'as' => 'cars.'], function () {
    Route::get('/',CarIndex::class)->name('categories');
    Route::get('/our-cars',OurCarIndex::class)->name('our-cars');
});

Route::get('/car/{car:slug}', CarInfo::class)->name('car-info');

Route::get('search/{category}/{energy}/{order}', DisplayResultCar::class )->name('search');

Route::get('/contact-us',ContactIndex::class)->name('contact-us');


// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

Route::group(['prefix' => 'dashboard', 'middleware' => 'auth'], function(){
    Route::get('/', function () {
        return view('dashboard');
    })->name('dashboard');

    Route::get('booking-cars', BookingCar::class)->name('vendor.booking-cars');

    Route::get('booking-cars/{slug}', BookingInformationCar::class)->name('vendor.booking-information-cars');
});

require __DIR__.'/auth.php';


Route::get('/posts', function () {
    $post = Post::find(1);

    $tag = Tag::find(1);

    dd($tag->posts);
});
