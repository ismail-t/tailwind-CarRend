<?php

namespace App\Traits;

trait UploaFile
{
    public function uploadFile($disk ,$file) : string
    {
        // $filenameWithExt = $file->getClientOriginalName();
        $filenameWithExt = $file->hashName();

        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

        // $extension = $file->getClientOriginalExtension();
        $extension = $file->extension();

        $fileNameToStore = $filename.'_'.time().'.'.$extension;

        $file->storeAs($disk,$fileNameToStore);

        return $fileNameToStore;
    }
}

?>
