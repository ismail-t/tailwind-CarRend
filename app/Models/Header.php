<?php

namespace App\Models;

use App\Models\Image;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Header extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'title',
        'teaser',
        'page_id'
    ];

    public function image(){
        return $this->morphOne(Image::class, 'imageable');
    }
}
