<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Car extends Model
{
    use HasFactory;

    // protected static function boot()
    // {
    //     parent::boot();

    //     static::creating(function ($car) {
    //         $car->slug = Str::slug($car->title) . "-" . date("Ymd");
    //     });
    // }

    protected $fillable = [
        'name',
        'slug',
        'price',
        'number',
        'wallet',
        'chair',
        'active',
        'category_id',
        'energy_id'
    ];



    public function getPriceDayAttribute()
    {
        return '$' . $this->price . ' /day';
    }

    public function getImageAttribute()
    {
        $defaultimage = null;

        foreach ($this->images as $image) {
            if ($image->default) {
                $defaultimage = $image;
            }
        }

        if(!$defaultimage)
        {
            $defaultimage = $this->images[0];
        }

        return $defaultimage->url_image;
    }

    public function getBookingPriceAttribute()
    {
        return '$' . $this->pivot->price;
    }

    public function getDaysRentalAttribute()
    {
        return date_create($this->pivot->start)->diff(date_create($this->pivot->end))->days;
    }

    public function scopeSearchByCategory(Builder $builder, $relation, $id, $order = 'asc')
    {
        return $builder->where($relation, $id);
    }

    public function scopeOrderByPrice(Builder $builder, $order ,$path = 'asc')
    {
        if ($path == "asc" || $path == "desc") {
            return $builder->orderby($order, $path);
        }
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function energy()
    {
        return $this->belongsTo(Energy::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, "bookings")->withPivot(['serial', 'price', 'start', 'end']);
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }
}
