<?php

namespace App\Http\Livewire\Vendor;

use App\Models\Car;
use App\Models\User;
use App\Traits\Pdf\Vendor\BookingInvoice;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Barryvdh\DomPDF\Facade\Pdf;

class BookingInformationCar extends Component
{
    use BookingInvoice;

    public $slug;

    public function mount($slug)
    {
        $this->slug = $slug;
    }

    public function getCarProperty()
    {
        $user = User::where('id', Auth::id())->with(['cars.images', 'cars.energy', 'cars.category','cars' => function($q) {
            $q->where('slug', $this->slug);
        } ])->first();

        if (count($user->cars) == 0) {
            abort(403);
        }

        return $user->cars[0];
    }

    public function downloadInvoice()
    {
        $user = Auth::user();

        view()->share('user', $user->name);
        $pdf = Pdf::loadView('layouts.pdf.invoice.bookingCar', ['user' => $user->name]);
        return $pdf->download($user->name . '.pdf');

        return $this->downloadInvoiceCar();
    }

    public function render()
    {
        return view('livewire.vendor.booking-information-car')->layout('layouts.app');
    }
}
