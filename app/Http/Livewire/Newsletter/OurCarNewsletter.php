<?php

namespace App\Http\Livewire\Newsletter;

use Livewire\Component;

class OurCarNewsletter extends Component
{
    public function render()
    {
        return view('livewire.newsletter.our-car-newsletter');
    }
}
