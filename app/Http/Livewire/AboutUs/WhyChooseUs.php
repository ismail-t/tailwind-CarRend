<?php

namespace App\Http\Livewire\AboutUs;

use Livewire\Component;

class WhyChooseUs extends Component
{
    public function render()
    {
        return view('livewire.about-us.why-choose-us');
    }
}
