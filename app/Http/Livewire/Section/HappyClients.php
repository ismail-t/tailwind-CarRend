<?php

namespace App\Http\Livewire\Section;

use Livewire\Component;

class HappyClients extends Component
{
    public function render()
    {
        return view('livewire.section.happy-clients');
    }
}
