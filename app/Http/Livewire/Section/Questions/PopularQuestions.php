<?php

namespace App\Http\Livewire\Section\Questions;

use Livewire\Component;

class PopularQuestions extends Component
{
    public function render()
    {
        return view('livewire.section.questions.popular-questions');
    }
}
