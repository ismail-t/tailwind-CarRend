<?php

namespace App\Http\Livewire\Section;

use Livewire\Component;

class FindYourVehicleByType extends Component
{
    public function render()
    {
        return view('livewire.section.find-your-vehicle-by-type');
    }
}
