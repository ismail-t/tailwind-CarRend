<?php

namespace App\Http\Livewire\Section;

use Livewire\Component;

class WhyChooseUs extends Component
{
    public function render()
    {
        return view('livewire.section.why-choose-us');
    }
}
