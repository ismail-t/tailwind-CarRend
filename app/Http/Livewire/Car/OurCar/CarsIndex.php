<?php

namespace App\Http\Livewire\Car\OurCar;

use App\Models\Car;
use Livewire\Component;
use Livewire\WithPagination;

class CarsIndex extends Component
{
    use WithPagination;

    protected $paginationTheme = 'tailwind';

    public $cars;

    public function getAllCarsProperty()
    {
        return Car::select('id', 'name', 'slug', 'chair', 'wallet', 'price')->with(['images', 'energy', 'category'])->paginate(3);
    }

    public function render()
    {
        return view('livewire.car.our-car.cars-index');
    }
}
