<?php

namespace App\Http\Livewire\Car\OurCar;

use Livewire\Component;

class Categories extends Component
{
    public function render()
    {
        return view('livewire.car.our-car.categories');
    }
}
