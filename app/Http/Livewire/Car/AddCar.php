<?php

namespace App\Http\Livewire\Car;

use App\Models\Car;
use App\Models\Category;
use App\Models\Energy;
use App\Traits\UploaFile;
use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;

class AddCar extends Component
{
    use WithFileUploads;
    use UploaFile;

    public $state = [
        'name' => '',
        'price' => '',
        'wallet' => 2,
        'chair' => 4,
        'door' => 2,
        'number' => 4,
    ];

    public $images ;
    public $category ;
    public $energy ;

    protected $rules = [
        'state.name' => 'required|min:3',
        'state.price' => 'required|integer',
        'state.wallet' => 'required|integer',
        'state.chair' => 'required|integer|min:2',
        'state.door' => 'required|integer|min:2',
        'state.number' => 'required|integer|min:1',
        'category' => 'exists:categories,id',
        'energy' => 'exists:energies,id',
        'images' => 'required',
    ];

    protected $messages = [
        'state.name.required' => 'The Name cannot be empty.',
        'state.name.min' => 'The Name min 3 characters.',
        'state.price.required' => 'The Price cannot be empty.',
        'state.wallet.required' => 'The Price cannot be empty.',
        'state.chair.required' => 'Evry Car Has a chiar.',
        'state.door.required' => 'Evry Car Has a chiar.',
        'category.required' => 'choose car\'s car',
        'energy.required' => 'choose car\'s car',
        'images' => 'you should uplaod a image of car.',
    ];

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function store()
    {
        $this->validate();

        $car = Car::create([
            'name' => $this->state['name'],
            'slug' => Str::slug($this->state['name']),
            'price' => $this->state['price'],
            'wallet' => $this->state['wallet'],
            'chair' => $this->state['chair'],
            'door' => $this->state['door'],
            'price' => $this->state['price'],
            'category_id' => $this->category,
            'energy_id' => $this->energy,
        ]);

        foreach ($this->images as $image) {

            $car->images()->create([
                "name" => $this->uploadFile('gallery' ,$image)
            ]);
        }

        reset($this->state);

        session()->flash('message', 'Post successfully updated.');
    }

    public function getCategoriesProperty()
    {
        $category = Category::select('id', 'name')->get();

        return $category;
    }

    public function getEnergiesProperty()
    {
        $energy = Energy::select('id', 'name')->get();

        return $energy;
    }

    public function render()
    {
        return view('livewire.car.add-car')->layout("layouts.base");
    }
}
