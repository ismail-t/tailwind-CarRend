<?php

namespace App\Http\Livewire\Car;

use Livewire\Component;

class OurCarIndex extends Component
{
    public function render()
    {
        return view('livewire.car.our-car-index')->layout('layouts.base');
    }
}
