<?php

namespace App\Http\Livewire\Car;

use Livewire\Component;

class CarIndex extends Component
{
    public function render()
    {
        return view('livewire.car.car-index')->layout("layouts.base");
    }
}
