<?php

namespace App\Http\Livewire\Car;

use App\Models\Car;
use Livewire\Component;

class CarInfo extends Component
{
    public $car;

    public function rentCar()
    {
        $this->emit('displayBookingForm', $this->car);
    }

    public function mount($car)
    {
        $this->car = Car::where('slug', $car)->with(['energy', 'images'])->first();
    }

    public function render()
    {
        return view('livewire.car.car-info')->layout('layouts.base');
    }
}
