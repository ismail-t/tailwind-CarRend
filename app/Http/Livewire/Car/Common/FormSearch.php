<?php

namespace App\Http\Livewire\Car\Common;

use App\Http\Livewire\Car\DisplayResultCar;
use App\Models\Car;
use App\Models\Category;
use App\Models\Energy;
use Livewire\Component;

class FormSearch extends Component
{
    public $form = [
        'category' => null,
        'energy' => null,
        'order' => null
    ];

    public $orders = ['asc', 'desc'];

    public function getCategoriesProperty()
    {
        return Category::select("id", "name")->get();
    }

    public function getEnergiesProperty()
    {
        return Energy::select("id", "name")->orderby('id', 'desc')->get();
    }

    public function search()
    {
        $cars = null;

        if ($this->form['category'] != null && $this->form['energy'] == null && !in_array($this->form['order'] ,$this->orders)) {

            $cars = Car::searchByCategory('category_id', $this->form['category'])->get();

        }elseif($this->form['category'] != null && $this->form['energy'] != null && !in_array($this->form['order'] ,$this->orders)) {

            $cars = Car::searchByCategory('category_id', $this->form['category'])->searchByCategory('energy_id', $this->form['energy'])->get();

        }elseif ($this->form['category'] != null && $this->form['energy'] != null && in_array($this->form['order'] ,$this->orders) ) {

            $cars = Car::searchByCategory('category_id', $this->form['category'])->searchByCategory('energy_id', $this->form['energy'])->orderby('price', $this->form['order'])->get();

        }elseif ($this->form['category'] != null && $this->form['energy'] == null && in_array($this->form['order'] ,$this->orders)) {

            $cars = Car::searchByCategory('category_id', $this->form['category'])->orderby('price', $this->form['order'])->get();
        }

        if($this->form['category'] == null && $this->form['energy'] != null && !in_array($this->form['order'] ,$this->orders)) {

            $cars = Car::searchByCategory('energy_id', $this->form['energy'])->get();

        }elseif ($this->form['category'] == null && $this->form['energy'] != null && in_array($this->form['order'] ,$this->orders) ) {

            $cars = Car::searchByCategory('energy_id', $this->form['energy'])->orderby('price', $this->form['order'])->get();

        }

        if ($this->form['category'] == null && $this->form['energy'] == null && in_array($this->form['order'] ,$this->orders) ) {

            $cars = Car::orderby('price', $this->form['order'])->get();
        }

        if ($cars == null) {
            $cars = Car::all();
        }

        return view('layouts.display-result', compact($cars));

        // return redirect()->route('search', ['category' => $this->form['category'], 'energy' => $this->form['energy'], 'order' => $this->form['order']]);
    }

    public function render()
    {
        return view('livewire.car.common.form-search');
    }
}
