<?php

namespace App\Http\Livewire;

use App\Models\Booking as ModelsBooking;
use App\Models\Car;
use Livewire\Component;

class Booking extends Component
{
    protected $listeners = ['displayBookingForm'];

    public $car;

    public $displayBookingForm = false;

    public $displayContract = false;

    public $confirmate = false;

    public $state = [
        'start_date' => null,
        'end_date' => null,
    ];

    protected $rules = [
        'state.start_date' => 'required',
        'state.end_date' => 'required',
    ];

    public function displayBookingForm(Car $car)
    {
        $this->car = $car;

        $this->displayBookingForm = true;
    }

    public function next()
    {
        $this->validate();

        if ($this->state["start_date"] < $this->state["end_date"]) {
            $this->confirmate = true;

            $this->displayContract = true;
        }
    }

    public function hideForm()
    {
        reset($this->state);

        $this->displayBookingForm = false;
    }

    public function getDaysRentalProperty()
    {
        return date_create($this->state["start_date"])->diff(date_create($this->state["end_date"]))->days;
    }

    public function getPriceProperty()
    {
        return $this->car->price * $this->daysRental;
    }

    public function rentalCar()
    {
        $serial = time() + ($this->price * 24 * 60 * 60);

        $booking = ModelsBooking::create([
            'serial' => $serial,
            'user_id' => auth()->user()->id,
            'car_id' => $this->car->id,
            'price' => $this->price,
            'start' => $this->state["start_date"],
            'end' => $this->state["end_date"],
            'days' => $this->daysRental
        ]);
    }

    public function render()
    {
        return view('livewire.booking');
    }
}
