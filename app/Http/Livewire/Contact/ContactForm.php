<?php

namespace App\Http\Livewire\Contact;

use App\Models\Contact;
use Livewire\Component;

class ContactForm extends Component
{
    public $state = [
        'name' => '',
        'email' => '',
        'phone' => '',
        'body' => ''
    ];

    public $loaded = false;

    protected $rules = [
        'state.name' => 'required|min:6',
        'state.email' => 'required|email',
        'state.phone' => 'required|min:10|max:14',
        'state.body' => 'required',
    ];

    protected $messages = [
        'state.name.required' => 'Name required',
        'state.name.min' => 'Min 6 characters',
        'state.email.required' => 'Email is required',
        'state.email.email' => 'Required Email format',
        'state.phone.required' => 'Phone Number is Required',
        'state.phone.min' => 'Min 6 characters',
        'state.phone.max' => 'Max 10 characters',
        'state.body.required' => 'Message is Required',
    ];


    public function sendMessage(){

        $this->validate();

        Contact::create([
            'name' => $this->state['name'],
            'email' => $this->state['email'],
            'phone' => $this->state['phone'],
            'body' => $this->state['body']
        ]);


        $this->reset('state');

        $this->loaded = true;
    }

    public function render()
    {
        return view('livewire.contact.contact-form');
    }
}

