<?php

namespace App\Http\Livewire\Cars;

use App\Models\Car;
use Livewire\Component;

class PopularCars extends Component
{
    public function getCarsProperty()
    {
        return Car::inRandomOrder()
                    ->limit(4)
                    ->with('energy')
                    ->with('images')
                    ->get();
    }
    public function render()
    {
        return view('livewire.cars.popular-cars');
    }
}
