const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    mode: 'jit',
    purge: [
        './resources/**/*.blade.php',
        './resources/**/*.js',
        './resources/**/*.vue',
    ],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
            colors: {
                'primary': "#247bdd",
                'first-gray': "#606060",
                'second-gray': "#333333",
                'third-gray' : "#9b9b9b",
                'forth-gray' : "#e1e1e1",
                'five-gray' : "#f7f7f7",
                'footer-gray' : "#2e2e2e",
            },

            fontFamily: {
                'poppins': ['poppins', 'sans-serif']
            },

            animation: {
                'show-menu': 'show-menu .6s ease-in-out forwards',
                'hidde-menu': 'hidde-menu .6s ease-in-out forwards',
                'show-timeStart': 'show-timeStart .3s ease-in-out forwards',
                'hidde-timeStart': 'hidde-timeStart .3s ease-in-out forwards',
            },


            keyframes: {
                'show-menu': {
                    '100%': { transform: 'translateX(0px)' },
                },
                'hidde-menu': {
                    '100%': { transform: 'translateX(-100%)' },
                },

                'show-timeStart': {
                    '100%': {
                        transform: 'translateY(0px)',
                        opacity: '1',
                        display: 'flex'
                    },
                },

                'hidde-timeStart': {
                    '100%': {
                        transform: 'translateY(40px)',
                        opacity: '0',
                        display: 'hidden'
                    }
                },
            },

            spacing: {
                '84': '1190px',
                '90': '1220px',
              }
        },
    },
    variants: {
        extend: {},
    },
    plugins: [],
}
